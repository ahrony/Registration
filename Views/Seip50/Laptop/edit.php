<?php
include_once("../../../vendor/autoload.php");

use App\Seip50\Laptop\Laptop;

$obj = new Laptop();
$data = $obj->prepare($_GET)->show();
?>
<html>
    <head>
        <link rel="stylesheet" type="text/css" href="../../../Views/Seip50/Laptop/css/style.css">
    </head>
    <body>
        <?php include_once ("./header.php");?>
        <h3>Input Your Data</h3>
        
        <?php
            if (isset($_SESSION['Message']) && !empty($_SESSION['Message'])) {
                echo $_SESSION['Message'];
                unset($_SESSION['Message']);
            }

        ?>
        <div class="k">
                <form action="update.php" method="post">
                    
                    <label>Name</label>&nbsp;
                    <input type="text" name="name" value="<?php echo $data['title'];?>">
                    <label for="lname">Model</label>
                    <input type="text" name="model" value="<?php echo $data['model'];?>">
                    <div id="dl">
                        <label for="lname">Serial</label>&nbsp;
                        <input type="text" name="sl" value="<?php echo $data['sl'];?>">
                        &nbsp;&nbsp;
                        <label for="lname">Color</label>&nbsp;&nbsp;&nbsp;
                        <input type="text" name="color" value="<?php echo $data['color'];?>">
                        <label for="lname">Price</label>&nbsp;&nbsp;
                        <input type="text" name="price" value="<?php echo $data['price'];?>">
                        <label for="lname">Purchase</label>&nbsp;
                        <input type="text" name="pdate" value="<?php echo $data['pdate'];?>">
                    </div>
                    <input type="submit" value="Submit">
                    <input type="hidden" name="id" value="<?php echo $data['id'];?>">
              </form>
            </div>
    </body>
</html>