
<?php
include_once ('../../../vendor/autoload.php');

$showObj = new App\Seip50\Registration\Registration();

$data = $showObj->assign($_GET)->show(); ?>

<html>
    <head>
    <link rel="stylesheet" type="text/css" href="css/style.css">
    </head>
    <body>
        <?php include_once ("./header.php");

if (isset($data) && !empty($data)) {
    ?>
    <table border="1" cellpadding="5">
        <tr>
            <th>Name</th>
            <th>Email</th>
        </tr>

        <tr>
            <td><?php echo ucwords($data['username']); ?></td>
            <td><?php echo ucfirst($data['email']); ?></td>
        </tr>


    </table>
    <?php
} else {
    $_SESSION['msg'] = '<font color="red">' . "Unauthorize User" . "</font>";
    header("location:index.php");
}
?>
<a href="index.php">View All</a>

</body>
</html>