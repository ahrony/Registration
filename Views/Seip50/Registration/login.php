<?php
session_start();
?>
<html>
    <head>
        <link rel="stylesheet" type="text/css" href="css/style.css">
    </head>
    
    <body>
        <?php include_once ("./header.php"); ?>
        <div id="main">
        <h2>Existing User? Sign In.</h2>
        <div id="frm">
        <form action="loginProcess.php" method="post">
            <?php
            
                if (isset($_SESSION['LogMsg']) && !empty($_SESSION['LogMsg'])){
                echo '<h2>' . $_SESSION['LogMsg'] . '</h2>';
                unset($_SESSION['LogMsg']);
                }
            ?>
            
            <label>Username :</label>
            <input type="text" name="username" autofocus>
            <?php
            
                if (isset($_SESSION['username']) && !empty($_SESSION['username'])){
                echo '<h2>' . $_SESSION['username'] . '</h2>';
                unset($_SESSION['username']);
                }
            ?>
            
            <label>Password :</label>
            <input type="password" name="pass">
            <?php
            
                if (isset($_SESSION['password']) && !empty($_SESSION['password'])){
                echo '<h2>' . $_SESSION['password'] . '</h2>';
                unset($_SESSION['password']);
                }
            ?>
            </br>
            <input type="submit" value="Login">
        </form>
            <h3><a href="create.php">SignUp</a> Here</h3>
  </div>
  
</div> 
        <a href="index.php">View All</a>
    </body>
</html>