<?php
session_start();
?>
<html>
    
<head>
    <link rel="stylesheet" type="text/css" href="css/style.css">
</head>
<body>
    <?php include_once ("./header.php");?>

<div id="main"> 
  <h2>Registration Form</h2>
  <div id="frm">
    <form method="POST" action="store.php">

            <label>Username: </label>
            <input type="text" name="username" value="<?php 
                if(isset($_SESSION['user']) && !empty($_SESSION['user'])){
                    echo $_SESSION['user'];
                    unset($_SESSION['user']);
                }
            ?>" autofocus>
            <?php 
                if(isset($_SESSION['username']) && !empty($_SESSION['username'])){
                    echo $_SESSION['username'];
                    unset($_SESSION['username']);
                }
            ?>
            <br>

            <label>Email Address</label>
            <input type="email" name="email" value="<?php 
                if(isset($_SESSION['emailvalue']) && !empty($_SESSION['emailvalue'])){
                    echo $_SESSION['emailvalue'];
                    unset($_SESSION['emailvalue']);
                }
            ?>">
            <?php 
                if(isset($_SESSION['email']) && !empty($_SESSION['email'])){
                    echo $_SESSION['email'];
                    unset($_SESSION['email']);
                }
            ?>
            <br>
            <label>Enter Password :</label></br>
            <div id="dl">
            <input type="password" name="pass" placeholder="Enter Password">
            <?php 
                if(isset($_SESSION['pass']) && !empty($_SESSION['pass'])){
                    echo $_SESSION['pass'];
                    unset($_SESSION['pass']);
                }
            ?>

            <input type="password" name="rpass" placeholder="Re Type Password">
            <?php 
                if(isset($_SESSION['rpass']) && !empty($_SESSION['rpass'])){
                    echo $_SESSION['rpass'];
                    unset($_SESSION['rpass']);
                }
            ?>
            </div>
            <br>
            <input type="checkbox" name="tc" value="y">Terms & Condition
            <?php 
                if(isset($_SESSION['tc']) && !empty($_SESSION['tc'])){
                    echo $_SESSION['tc'];
                    unset($_SESSION['tc']);
                }
            ?>
            <br>
            <input type="submit" value="Registration">
    </form>
    
  </div>
  
</div> 
<a href="index.php">View All</a>
</body>
</html>