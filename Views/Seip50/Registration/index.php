<?php
include_once ('../../../vendor/autoload.php');

$dataObj = new App\Seip50\Registration\Registration();

if(!isset($_SESSION['loginUser']) && empty($_SESSION['loginUser'])){
    $_SESSION['logMsg'] = "Login First";
    header("location:login.php");
}


$data = $dataObj->index();

if (isset($_SESSION['msg']) && !empty($_SESSION['msg'])) {
    echo '<h2>' . $_SESSION['msg'] . '</h2>';
    unset($_SESSION['msg']);
}
?>
<html>
<head>
    <link rel="stylesheet" type="text/css" href="css/style.css">
</head>
<body>
    <?php include_once ("./header.php");?>
<h2>Active Data</h2>
<table id="tableData" border="1" cellpadding="5">
            <thead>
                <tr>
                    <th>SL</th>
                    <th>Name</th>
                    <th>Mobile Number</th>
                    <th colspan="3">Action</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $sl = 1;
                if (isset($data) && !empty($data)) {
                    foreach ($data as $item) {
                        ?>
                        <tr>
                            <td><?php echo $sl++; ?></td>
                            <td><?php echo ucwords($item['username']); ?></td>
                            <td><?php echo ucfirst($item['email']) ?></td>
                            <td><a href="show.php?id=<?php echo $item['u_id'] ?>">View</a></td>
                            <td><a href="edit.php?id=<?php echo $item['u_id'] ?>">Edit</a></td>
                            <td><a href="trush.php?id=<?php echo $item['u_id'] ?>">Delete</a></td>
                        
                        </tr>
                        <?php
                    }
                } else {
                    echo '<tr><td colspan="8"><font color="red">Empty Database</font></td></tr>';
                }
                ?>  
            </tbody>
        </table>

<br>

<?php
if(isset($_SESSION['isAdmin']) && !empty($_SESSION['isAdmin'])){
    if($_SESSION['isAdmin']==1){
        echo '<a href="create.php">Add New</a> ||';
    }
}
?>





<a href="trushed.php">Trushed User</a>
</body>
</html>
