<?php
include_once ('../../../vendor/autoload.php');

$loginObj = new App\Seip50\Registration\Registration();

if($_SERVER['REQUEST_METHOD'] == 'POST'){
    
    $data = $loginObj->assign($_POST)->login();
   
        if(isset($data) && !empty($data)){
           $_SESSION['loginUser'] = $data['username'];
           $_SESSION['isAdmin'] = $data['isAdmin'];
           
            header('location:index.php');
        }else{
            $_SESSION['logMsg'] = 'You Type Wrong information';
            header('location:login.php');
        }
    }else{
        $_SESSION['logMsg'] = 'Type Username and Password First';
        header('location:login.php');
}