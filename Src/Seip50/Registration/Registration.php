<?php
namespace App\Seip50\Registration;
use PDO;

class Registration {
    
    public $id = '';
    public $username = '';
    public $email = '';
    public $pass = '';
    public $rpass = '';
    public $data = '';
    public $tc = '';
    public $user = "root";
    public $password = '';


    public function __construct() {
        session_start();
        $this->conn = new PDO('mysql:host=localhost;dbname=reg', $this->user, $this->password);
        //$Vdate = date("Y-m-d g:i:s");
        date_default_timezone_set("Asia/Dhaka"); 
    }
    
    // assgin value
    public function assign($data) {
        if (!empty($data['username'])) {
            $this->username = $data['username'];
        } else {
            $_SESSION['username'] = '<font color="red">' . "Username Required" . '</font>';
        }
        
        if (!empty($data['email'])) {
            $this->email = $data['email'];
        } else {
            $_SESSION['email'] = '<font color="red">' . "Email Required" . '</font>';
        }

        if (!empty($data['pass'])) {
            $this->pass = $data['pass'];
        } else {
            $_SESSION['pass'] = '<font color="red">' . "Password Required" . '</font>';
        }
        
        if (!empty($data['rpass'])) {
            $this->rpass = $data['rpass'];
        } else {
            $_SESSION['rpass'] = '<font color="red">' . "Repeated Password Required" . '</font>';
        }

        if (array_key_exists('id', $data)) {
            $this->id = $data['id'];
        }

        if (!empty($data['tc'])) {
            $this->tc = $data['tc'];
        } else {
            $this->tc = 'n';
            $_SESSION['tc'] = '<font color="red">' . "Please Check Terms & Condition" . '</font>';
        }

        return $this;
    }
    
    // store value into database 
    public function store() {
        
        
        
        if ($this->tc == 'y'){
            try {
                $query = "INSERT INTO `reg`.`users` (username,email,pass,tc,isActive,createdat,isAdmin,u_id)
                    VALUES(:username,:email,:pass,:tc,:is_active,:created_at,:is_admin,:u_id)";
                $q = $this->conn->prepare($query);
                $q->execute(array(
                    ':username' => $this->username,
                    ':email'    =>  $this->email,
                    ':pass'     =>  $this->pass,
                    ':tc'     =>  $this->tc,                 
                    ':is_active'    => '1',
                    ':created_at' => date("Y-m-d G:i:s"),
                    ':is_admin' =>'1',
                    ':u_id' => uniqid()
                ));
                $_SESSION['msg'] = '<font color="green">' . "Successfully Registration" . '</font>';
                unset($_SESSION['user']);
                unset($_SESSION['emailvalue']);
                header("LOCATION: index.php");
            } catch (PDOException $e) {
                echo 'Error: ' . $e->getMessage();
            }
        } else {
            header("LOCATION: create.php");
        }
        return $this;
    }
    
    // get single data
    public function show() {
        try {
            $qry = "SELECT * FROM `users` WHERE `u_id` = '" . $this->id . "'";
            $q = $this->conn->query($qry) or die(mysql_error());
            $row = $q->fetch(PDO::FETCH_ASSOC);
        } catch (Exception $ex) {
            echo 'Error: ' . $ex->getMessage();
        }
        return $row;
        return $this;
    }
    
    
    // get all data from database
    public function index() {
        try {
            $qry = "SELECT * FROM `reg`.`users` WHERE is_deleted=0";
            $q = $this->conn->query($qry) or die(mysql_error());
            while ($row = $q->fetch(PDO::FETCH_ASSOC)) {
                $this->data[] = $row;
            }
        } catch (Exception $ex) {
            echo 'Error: ' . $ex->getMessage();
        }
        return $this->data;

        return $this;
    }
    
    
    // get all data from database
    public function tIndex() {
        try {
            $qry = "SELECT * FROM `users` WHERE is_deleted=1 ORDER BY `deletedat` DESC";
            $q = $this->conn->query($qry) or die(mysql_error());
            while ($row = $q->fetch(PDO::FETCH_ASSOC)) {
                $this->data[] = $row;
            }
        } catch (Exception $ex) {
            echo 'Error: ' . $ex->getMessage();
        }
        return $this->data;

        return $this;
    }
    
    // trush
    public function trush() {
        echo 'number=' . $this->id;

        try {

            $upQry = "UPDATE users SET 
                        is_deleted = :is_deleted,
                        deletedat = :deletedat
                        WHERE u_id = :uid";
            $q = $this->conn->prepare($upQry);
            $q->execute(array(
                        ':is_deleted' => '1',
                        ':deletedat' => date("Y-m-d G:i:s"),
                        ':uid' => $this->id
                    )) or die('Error While Deleted');

            $_SESSION['msg'] = '<font color="green">' . "Successfully Deleted..." . '</font>';
            header("location: index.php");
        } catch (Exception $ex) {
            echo 'Error: ' . $ex->getMessage();
        }

        return $this;
    }
    
    public function login() {
        
        if(!empty($this->username) && !empty($this->pass)){
            try{
            $query = "SELECT  * FROM `reg`.`users` WHERE
                username = '" .$this->username . "' &&
                pass = '" . $this->pass . "' &&
                isActive = '1' ";
            
            $q = $this->conn->query($query) or die(mysql_error());
            $row = $q->fetch(PDO::FETCH_ASSOC);
            $_SESSION['logMsg'] = 'Succesfully Login';
            
            return $row;
            return $this;
            
        } catch (Exception $ex){
            $_SESSION['logMsg'] = 'Login First';
            echo $ex->getMessage();
        }
        
    }else {
        header('location:login.php');
    }   
}
}
